(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user-user-module"],{

/***/ "./src/app/user/resolvers/user.guard.ts":
/*!**********************************************!*\
  !*** ./src/app/user/resolvers/user.guard.ts ***!
  \**********************************************/
/*! exports provided: UserGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserGuard", function() { return UserGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user.service */ "./src/app/user/services/user.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UserGuard = /** @class */ (function () {
    function UserGuard(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    UserGuard.prototype.resolve = function (route) {
        var _this = this;
        return this.userService.getUser(route.params['id']).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(function (error) {
            _this.router.navigate(['/restaurants']);
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(null);
        }));
    };
    UserGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], UserGuard);
    return UserGuard;
}());



/***/ }),

/***/ "./src/app/user/services/user.service.ts":
/*!***********************************************!*\
  !*** ./src/app/user/services/user.service.ts ***!
  \***********************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserService = /** @class */ (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.getUser = function (id) {
        if (id === void 0) { id = null; }
        console.log(id);
        var ide = id == null ? 'me' : id;
        console.log(ide);
        return this.http.get("users/" + ide).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (resp) {
            var r = resp.user;
            r.avatar = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].baseUrl + "/" + r.avatar;
            return r;
        }));
    };
    UserService.prototype.changeUserProfile = function (user) {
        return this.http.put("users/me", {
            "name": user.name, "email": user.email
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (resp) {
            var r = resp;
            return r;
        }));
    };
    UserService.prototype.changeAvatarProfile = function (avatar) {
        return this.http.put("users/me/avatar", {
            "avatar": avatar
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (resp) {
            var r = resp;
            return r;
        }));
    };
    UserService.prototype.changePasswordProfile = function (user) {
        return this.http.put("users/me/password", {
            "password": user.password
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (resp) {
            var r = resp;
            return r;
        }));
    };
    UserService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UserService);
    return UserService;
}());



/***/ }),

/***/ "./src/app/user/user-form/user-form.component.html":
/*!*********************************************************!*\
  !*** ./src/app/user/user-form/user-form.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n  <div class=\"container\">\n    <form novalidate class=\"mt-4\"\n    #profileForm=\"ngForm\" (ngSubmit)=\"changeProfile()\" >\n      <legend>Edit profile</legend>\n\n      <div class=\"form-group\">\n        <label for=\"correo\">Email:</label>\n        <input type=\"email\" class=\"form-control\" name=\"email\" id=\"email\" \n        [(ngModel)]=\"user.email\"  #emailModel=\"ngModel\" required  email placeholder=\"Email\"\n        [ngClass]=\"validClasses(emailModel, 'is-valid', 'is-invalid')\">\n        <div *ngIf=\"emailModel.touched && emailModel.invalid\" class=\"alert alert-danger\">\n          Email is required and  must be correct\n        </div>\n        <label for=\"name\">Name:</label>\n        <input type=\"text\" class=\"form-control\" name=\"nameUser\" id=\"name\" placeholder=\"Name\"\n        [(ngModel)]=\"user.name\"  #nameModel=\"ngModel\" \n        required [ngClass]=\"validClasses(nameModel, 'is-valid', 'is-invalid')\" >\n        <div *ngIf=\"nameModel.touched && nameModel.invalid\" class=\"alert alert-danger\">\n          Name is required\n        </div>\n      </div>\n      <div >\n        <p class=\"text-danger\" * id=\"errorInfo2\">Magnifico</p>\n        <p class=\"text-success\" id=\"okInfo2\"></p>\n      </div>\n      \n      <button type=\"submit\" [disabled]=\"profileForm.invalid\" class=\"btn btn-primary\">Edit profile</button>\n    </form>\n    <form  id=\"form-avatar\" class=\"mt-4\"  #avatarForm=\"ngForm\" (ngSubmit)=\"changeAvatar()\">\n      <legend>Edit avatar</legend>\n\n      <div class=\"form-group\">\n        <input type=\"file\" class=\"form-control\" id=\"image\" name=\"image\" #fileImage (change)=\"changeImage(fileImage)\"\n        required>\n      </div>\n      <div class=\"row\">\n        <div class=\"col\" class=\"w-100\">\n            <img class=\"w-100\" id=\"avatar\" [src]=\"user.avatar\" alt=\"\">\n        </div>\n        <div class=\"col\">\n            <img *ngIf=\"change\" class=\" w-100\" [src]=\"newImage\"  alt=\"\" id=\"imgPreview\">\n        </div>\n      </div>\n      <p class=\"text-danger\" id=\"errorInfo2\"></p>\n      <p class=\"text-success\" id=\"okInfo2\"></p>\n      <button type=\"submit\"  class=\"btn btn-primary\">Edit avatar</button>\n    </form>\n    <form  id=\"form-password\" class=\"mt-4\" #passwordForm=\"ngForm\" (ngSubmit)=\"changePassword()\">\n      <legend>Edit password</legend>\n\n      <div class=\"form-group\">\n        <label for=\"password\">Password:</label>\n        <input type=\"password\" class=\"form-control\" name=\"password\" id=\"password\" placeholder=\"Password\"\n        [(ngModel)]=\"user.password\"  #passwordModel=\"ngModel\" \n        required [ngClass]=\"validClasses(passwordModel, 'is-valid', 'is-invalid')\"\n        >\n        <div *ngIf=\"passwordModel.touched && passwordModel.invalid\" class=\"alert alert-danger\">\n          Password is required\n        </div>\n        <label for=\"password2\">Repeat password:</label>\n        <input type=\"password\" class=\"form-control\" name=\"password2\" id=\"password2\" placeholder=\"Repeat password\"\n        [(ngModel)]=\"user.repeatPassword\"  #rpasswordModel=\"ngModel\" \n        fsConfirmPassword=\"password\"\n        required [ngClass]=\"validClasses(rpasswordModel, 'is-valid', 'is-invalid')\">\n        <div *ngIf=\"rpasswordModel.touched && rpasswordModel.invalid\" class=\"alert alert-danger\">\n          Repeat password must be equal to the original password\n        </div>\n      </div>\n      <p class=\"text-danger\" id=\"errorInfo3\"></p>\n      <p class=\"text-success\" id=\"okInfo3\"></p>\n      <button type=\"submit\" [disabled]=\"passwordForm.invalid\"  class=\"btn btn-primary\">Edit password</button>\n    </form>\n  </div>"

/***/ }),

/***/ "./src/app/user/user-form/user-form.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/user/user-form/user-form.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3VzZXIvdXNlci1mb3JtL3VzZXItZm9ybS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/user/user-form/user-form.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/user/user-form/user-form.component.ts ***!
  \*******************************************************/
/*! exports provided: UserFormComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserFormComponent", function() { return UserFormComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/user.service */ "./src/app/user/services/user.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UserFormComponent = /** @class */ (function () {
    function UserFormComponent(router, route, userService) {
        this.router = router;
        this.route = route;
        this.userService = userService;
        this.change = false;
    }
    UserFormComponent.prototype.ngOnInit = function () {
        this.user = this.route.snapshot.data['user'];
        console.log(this.user);
        this.user.repeatPassword;
    };
    UserFormComponent.prototype.validClasses = function (ngModel, validClass, errorClass) {
        var _a;
        return _a = {},
            _a[validClass] = ngModel.touched && ngModel.valid,
            _a[errorClass] = ngModel.touched && ngModel.invalid,
            _a;
    };
    UserFormComponent.prototype.changeProfile = function () {
        if (this.profileForm.invalid)
            return;
        this.userService.changeUserProfile(this.user).subscribe(function (rest) { return console.log("profile cambiada"); }, function (error) { return console.error(error); });
    };
    UserFormComponent.prototype.changeImage = function (fileInput) {
        var _this = this;
        if (!fileInput.files || fileInput.files.length === 0) {
            return;
        }
        var reader = new FileReader();
        reader.readAsDataURL(fileInput.files[0]);
        reader.addEventListener('loadend', function (e) {
            _this.newImage = reader.result;
            _this.change = true;
        });
    };
    UserFormComponent.prototype.changeAvatar = function () {
        var _this = this;
        if (this.avatarForm.invalid)
            return;
        this.userService.changeAvatarProfile(this.newImage).subscribe(function (rest) {
            console.log("magnifico tio");
            _this.user.avatar = _this.newImage;
            _this.newImage = "";
            _this.change = false;
        }, function (error) { return console.log(error); });
    };
    UserFormComponent.prototype.changePassword = function () {
        if (this.passwordForm.invalid)
            return;
        this.userService.changePasswordProfile(this.user).subscribe(function (rest) { return console.log("Contraseña cambiada"); }, function (error) { return console.log(error); });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('profileForm'),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], UserFormComponent.prototype, "profileForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('avatarForm'),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], UserFormComponent.prototype, "avatarForm", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('passwordForm'),
        __metadata("design:type", _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NgForm"])
    ], UserFormComponent.prototype, "passwordForm", void 0);
    UserFormComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'fs-user-form',
            template: __webpack_require__(/*! ./user-form.component.html */ "./src/app/user/user-form/user-form.component.html"),
            styles: [__webpack_require__(/*! ./user-form.component.scss */ "./src/app/user/user-form/user-form.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _services_user_service__WEBPACK_IMPORTED_MODULE_3__["UserService"]])
    ], UserFormComponent);
    return UserFormComponent;
}());



/***/ }),

/***/ "./src/app/user/user-page/user-page.component.html":
/*!*********************************************************!*\
  !*** ./src/app/user/user-page/user-page.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div id=\"profile\">\n    <div class=\"row mt-4\">\n      <div class=\"col-3\">\n        <img class=\"w-100\" id=\"avatar\" [src]=\"user.avatar\" alt=\"\">\n      </div>\n      <div class=\"col-9\">\n        <h4>{{user.name}}</h4>\n        <h4>\n          <small class=\"text-muted\">{{user.email}}</small>\n        </h4>\n        <p>\n          <a  [routerLink]=\"['/profile/edit']\" routerLinkActive=\"active\" *ngIf=\"user.me\" >Edit profile</a>\n        </p>\n      </div>\n    </div>  \n  </div>\n\n\n  <div  class=\"mt-4\" id=\"map\">\n    <mgl-map [style]=\"'mapbox://styles/mapbox/streets-v11'\"\n    [zoom]=\"[zoom]\" [center]=\"[lng,lat]\">\n    <mgl-marker [lngLat]=\"[lng, lat]\"></mgl-marker>\n    </mgl-map>\n  </div>\n\n  </div>\n"

/***/ }),

/***/ "./src/app/user/user-page/user-page.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/user/user-page/user-page.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "mgl-map {\n  height: 400px;\n  width: 100%; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy90b25pZGVmZXovZGF3L2R3ZWNfcmVwb3NpdG9yeS9mb29kc2NvcmUvc3JjL2FwcC91c2VyL3VzZXItcGFnZS91c2VyLXBhZ2UuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBVSxjQUFhO0VBQUUsWUFBVyxFQUNuQyIsImZpbGUiOiJzcmMvYXBwL3VzZXIvdXNlci1wYWdlL3VzZXItcGFnZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIm1nbC1tYXAgeyBoZWlnaHQ6IDQwMHB4OyB3aWR0aDogMTAwJTtcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/user/user-page/user-page.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/user/user-page/user-page.component.ts ***!
  \*******************************************************/
/*! exports provided: UserPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPageComponent", function() { return UserPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserPageComponent = /** @class */ (function () {
    function UserPageComponent(router, route) {
        this.router = router;
        this.route = route;
        this.lat = 38.4039418;
        this.lng = -0.5288701;
        this.zoom = 17;
    }
    UserPageComponent.prototype.ngOnInit = function () {
        this.user = this.route.snapshot.data['user'];
        console.log(this.user);
        this.lat = this.user.lat;
        this.lng = this.user.lng;
    };
    UserPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'fs-user-page',
            template: __webpack_require__(/*! ./user-page.component.html */ "./src/app/user/user-page/user-page.component.html"),
            styles: [__webpack_require__(/*! ./user-page.component.scss */ "./src/app/user/user-page/user-page.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]])
    ], UserPageComponent);
    return UserPageComponent;
}());



/***/ }),

/***/ "./src/app/user/user-routing.module.ts":
/*!*********************************************!*\
  !*** ./src/app/user/user-routing.module.ts ***!
  \*********************************************/
/*! exports provided: UserRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserRoutingModule", function() { return UserRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _user_page_user_page_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user-page/user-page.component */ "./src/app/user/user-page/user-page.component.ts");
/* harmony import */ var _resolvers_user_guard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./resolvers/user.guard */ "./src/app/user/resolvers/user.guard.ts");
/* harmony import */ var _user_form_user_form_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user-form/user-form.component */ "./src/app/user/user-form/user-form.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var routes = [
    {
        path: 'me',
        component: _user_page_user_page_component__WEBPACK_IMPORTED_MODULE_2__["UserPageComponent"],
        pathMatch: 'full',
        resolve: {
            user: _resolvers_user_guard__WEBPACK_IMPORTED_MODULE_3__["UserGuard"]
        }
    },
    {
        path: 'edit',
        pathMatch: 'full',
        component: _user_form_user_form_component__WEBPACK_IMPORTED_MODULE_4__["UserFormComponent"],
        resolve: {
            user: _resolvers_user_guard__WEBPACK_IMPORTED_MODULE_3__["UserGuard"]
        }
    },
    {
        path: ':id',
        component: _user_page_user_page_component__WEBPACK_IMPORTED_MODULE_2__["UserPageComponent"],
        pathMatch: 'full',
        resolve: {
            user: _resolvers_user_guard__WEBPACK_IMPORTED_MODULE_3__["UserGuard"]
        }
    }
];
var UserRoutingModule = /** @class */ (function () {
    function UserRoutingModule() {
    }
    UserRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], UserRoutingModule);
    return UserRoutingModule;
}());



/***/ }),

/***/ "./src/app/user/user.module.ts":
/*!*************************************!*\
  !*** ./src/app/user/user.module.ts ***!
  \*************************************/
/*! exports provided: UserModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModule", function() { return UserModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _user_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user-routing.module */ "./src/app/user/user-routing.module.ts");
/* harmony import */ var _user_page_user_page_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./user-page/user-page.component */ "./src/app/user/user-page/user-page.component.ts");
/* harmony import */ var ngx_mapbox_gl__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-mapbox-gl */ "./node_modules/ngx-mapbox-gl/fesm5/ngx-mapbox-gl.js");
/* harmony import */ var _user_form_user_form_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-form/user-form.component */ "./src/app/user/user-form/user-form.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _validators_validators_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../validators/validators.module */ "./src/app/validators/validators.module.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var UserModule = /** @class */ (function () {
    function UserModule() {
    }
    UserModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _user_routing_module__WEBPACK_IMPORTED_MODULE_2__["UserRoutingModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                ngx_mapbox_gl__WEBPACK_IMPORTED_MODULE_4__["NgxMapboxGLModule"].withConfig({
                    accessToken: 'pk.eyJ1IjoiYWRpNyIsImEiOiJjanByNGN1Y3QwcW8yNDJuOWwyZW5rOWIxIn0.fpriima12X9VJOoaeZczmg'
                }),
                _validators_validators_module__WEBPACK_IMPORTED_MODULE_7__["ValidatorsModule"]
            ],
            declarations: [
                _user_page_user_page_component__WEBPACK_IMPORTED_MODULE_3__["UserPageComponent"],
                _user_form_user_form_component__WEBPACK_IMPORTED_MODULE_5__["UserFormComponent"]
            ]
        })
    ], UserModule);
    return UserModule;
}());



/***/ })

}]);
//# sourceMappingURL=user-user-module.js.map