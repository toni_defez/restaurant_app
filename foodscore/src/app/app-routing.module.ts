import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutActivateGuard } from './guards/logout-activate.guard';
import { LoginActivateGuard } from './guards/login-activate.guard';

const routes: Routes = [
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule', canActivate: [LogoutActivateGuard]},
  { path: 'restaurants', loadChildren: './restaurant/restaurant.module#RestaurantModule', canActivate: [LoginActivateGuard]},
  { path: 'profile', loadChildren:'./user/user.module#UserModule', canActivate: [LoginActivateGuard]},
  { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/auth/login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
