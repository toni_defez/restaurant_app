import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BaseUrlInterceptor } from './interceptors/base-url.interceptor';
import { AuthTokenInterceptor } from './interceptors/auth-token.interceptor';
import { MenuModule } from './menu/menu.module';
import { GoogleLoginModule } from './shared/google-login/google-login.module';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { FacebookLoginModule } from './shared/facebook-login/facebook-login.module';
import { ConfirmModalComponent } from './modals/confirm-modal/confirm-modal.component';
import { ModalsModule } from './modals/modals.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MenuModule,
    GoogleLoginModule.forRoot('807741492839-d01ql2r18gqiipf9amrfa891oh4rgtgt.apps.googleusercontent.com'),
    FacebookLoginModule.forRoot({
      app_id: '570446563419741',
      version: 'v3.2'
    }),
    ModalsModule,
    NgbModule
   
  ],
 
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BaseUrlInterceptor,
      multi: true,
    }, {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthTokenInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
