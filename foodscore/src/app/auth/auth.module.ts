import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginPageComponent } from './login-page/login-page.component';
import { FormsModule } from '@angular/forms';
import { RegisterPageComponent } from './register-page/register-page.component';
import { GoogleLoginModule } from '../shared/google-login/google-login.module';
import { FacebookLoginModule } from '../shared/facebook-login/facebook-login.module';
import { ValidatorsModule } from '../validators/validators.module';


@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    GoogleLoginModule.forRoot('807741492839-apvrne0j65a0hfv4o6n9fj80ko7omv35.apps.googleusercontent.com'),
    FacebookLoginModule.forRoot({
      app_id: '264506843964721',
      version: 'v3.2'
    }),
    ValidatorsModule
  ],
  declarations: [
    LoginPageComponent,
    RegisterPageComponent
  ]
})
export class AuthModule { }

