import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { GeolocationService } from '../services/geolocation.service';

@Component({
  selector: 'fs-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  email = '';
  password = '';
  showError = false;
  name = "";
  avatar = "";

  constructor(private authService: AuthService, private router: Router,
    private ngZone: NgZone) { }

  loggedGoogle(user: gapi.auth2.GoogleUser) {
    console.log("hola");
    // Send this token to your server for register / login
    console.log(user.getAuthResponse().id_token);
    this.ngZone.run(() => {
      //aqui llamare al servicio encargado de el servicio
      GeolocationService.getLocation().then(position => {
          let token = user.getAuthResponse().id_token;
          let lat = +(<Position>position).coords.latitude;
          let lng = +(<Position>position).coords.longitude;
          
          this.authService.loginGoogle(lat,lng,token).subscribe(
            () => this.router.navigate(['/restaurants']),
            error => this.showError = true
          );
          
      })
    });
  }

  loggedFacebook(resp: FB.LoginStatusResponse) {
    // Send this to your server

    this.ngZone.run(()=>{
      console.log(resp.authResponse.accessToken);
    
      GeolocationService.getLocation().then(position => {
        let token = resp.authResponse.accessToken;
        let lat = +(<Position>position).coords.latitude;
        let lng = +(<Position>position).coords.longitude;
        
        this.authService.loginFacebook(lat,lng,token).subscribe(
          () => this.router.navigate(['/restaurants']),
          error => this.showError = true
        );
        
    })
   
  })



  }

  errorGoogle(error) {
    console.error(error);
  }

  ngOnInit() {
  }

  login() {
    this.showError = false;
    this.authService.login(this.email, this.password).subscribe(
      () => this.router.navigate(['/restaurants']),
      error => this.showError = true
    );
  }
}
