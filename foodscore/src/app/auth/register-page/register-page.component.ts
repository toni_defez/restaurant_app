import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from '../interfaces/user.interface';
import { NgForm, NgModel } from '@angular/forms';
import { GeolocationService } from '../services/geolocation.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import swal, { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'fs-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})

export class RegisterPageComponent implements OnInit {

  newUser: User;
  
  @ViewChild('Form') Form: NgForm;
  constructor(private userService: AuthService, private router: Router) { }


  ngOnInit() {
  
    swal.isVisible();
    
    
    // Inicializamos los campos del formulario
    this.initializeNewUser();

    // Lat y Lng
    GeolocationService.getLocation().then(position => {
      let inputLat = <HTMLInputElement>document.getElementById('lat');
      let inputLng = <HTMLInputElement>document.getElementById('lng');
      // Visualiza
      inputLat.value = (<Position>position).coords.latitude.toString();
      inputLng.value = (<Position>position).coords.longitude.toString();
      // Setea
      this.newUser.lat = (<Position>position).coords.latitude;
      this.newUser.lng = (<Position>position).coords.longitude;
    }).catch(error => {
        console.error('No se ha podido obtener la posición del usuario registrado: ', error);
    });
  }

  addUser(form){
    // Recogemos los datos del usuario
    this.newUser = {
        name: form.value.name,
        email: form.value.email,
        password: form.value.password,
        avatar: this.newUser.avatar,
        lat: form.value.lat,
        lng: form.value.lng
    };

    let errFormReg = <HTMLElement>document.getElementById('errorInfo');
    // Valida que las contraseñas sean iguales
    if(this.newUser.password !== form.value.repeatPassword){
        errFormReg.innerText = 'Las contraseñas deben coincidir';
        return;
    }
    console.log("Hola");

    // Registramos el usuario
    this.userService.register(this.newUser).subscribe(
      async () => {
        await swal({
          title: "Good job!",
          text: "You clicked the button!"
        }).then( ()=>this.router.navigate(['/restaurants']))
    },
      error => console.log("Error al hacer el registro: ", error)
    );
  }

  changeImage(fileInput: HTMLInputElement) {
    if (!fileInput.files || fileInput.files.length === 0) { return; }
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', e => {
      this.newUser.avatar = <string>reader.result;
    });
  }

  private initializeNewUser() {
    this.newUser = {
        name: '',
        email: '',
        password: '',
        repeatPassword: '',
        avatar: '',
        lat: 0,
        lng: 0
    };
}


  // Validar inputs formulario
  validClasses(ngModel: NgModel, validClass: string, errorClass: string) {
    return {
        [validClass]: ngModel.touched && ngModel.valid,
        [errorClass]: ngModel.touched && ngModel.invalid
    };
}
}
