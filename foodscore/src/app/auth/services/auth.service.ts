import { Injectable, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { User } from '../interfaces/user.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  logged = false;
  loginChange$ = new EventEmitter<boolean>();

  constructor(private http: HttpClient) { }

  private setLogged(logged: boolean) {
    this.logged = logged;
    this.loginChange$.emit(logged);
  }

  login(email: string, password: string): Observable<void> {
    return this.http.post<{accessToken: string}>('auth/login', {email, password}).pipe(
      map(r => {
        localStorage.setItem('fs-token', r.accessToken);
        this.setLogged(true);
      })
    );
  }

  loginGoogle(lat:number,lng:number,token:string):Observable<void>{
    return this.http.post<{accessToken: string}>('auth/google', {lat,lng, token}).pipe(
      map(r => {
        localStorage.setItem('fs-token', r.accessToken);
        this.setLogged(true);
      })
    );
  }

  loginFacebook(lat:number,lng:number,token:string):Observable<void>{
    return this.http.post<{accessToken: string}>('auth/facebook', {lat,lng, token}).pipe(
      map(r => {
        localStorage.setItem('fs-token', r.accessToken);
        this.setLogged(true);
      })
    );
  }
  
  register(user :User):Observable<User>{
    return this.http.post<{user:User}>(`auth/register`,user).pipe(
      map(resp=>{
        const r = resp.user;
        return r;
      })
    )
  }

  logout() {
    localStorage.removeItem('fs-token');
    this.setLogged(false);
  }

  isLogged(): Observable<boolean> {
    if (this.logged) { return of(true); }
    if (!localStorage.getItem('fs-token')) { return of(false); }
    return this.http.get('auth/validate').pipe(map(() => {
      this.setLogged(true);
      return true;
    }), catchError(error => of(false)));
  }
}
