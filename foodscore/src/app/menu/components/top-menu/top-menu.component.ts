import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'fs-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {
  logged = false;

  constructor(private authService: AuthService, private router: Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.authService.loginChange$.subscribe(logged => this.logged = logged);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/auth/login']);
  }
}
