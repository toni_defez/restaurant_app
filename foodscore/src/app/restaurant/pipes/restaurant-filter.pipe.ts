import { Pipe, PipeTransform } from '@angular/core';
import { Restaurant } from '../interfaces/restaurant.interface';

@Pipe({
  name: 'restaurantFilter'
})
export class RestaurantFilterPipe implements PipeTransform {

  transform(rests: Restaurant[], orderByName = false, showOpen = false, search = ''): any {
    let result = [...rests];
    const weekDay = (new Date()).getDay();
    if (orderByName) { result = result.sort((r1, r2) => r1.name.localeCompare(r2.name)); }
    if (showOpen) { result = result.filter(r => r.daysOpen.toString().includes(weekDay.toString())); }
    return search ? result.filter(r => r.name.toLocaleLowerCase().includes(search.toLocaleLowerCase())) : result;
  }

}
