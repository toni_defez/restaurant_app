import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Restaurant } from '../interfaces/restaurant.interface';
import { RestaurantService } from '../services/restaurant.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestaurantResolver implements Resolve<Restaurant> {
  constructor(private restService: RestaurantService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Restaurant> {
    return this.restService.getRestaurant(route.params['id']).pipe(
      catchError(error => {
        this.router.navigate(['/restaurants']);
        return of(null);
      })
    );
  }
}
