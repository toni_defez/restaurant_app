import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Restaurant } from '../interfaces/restaurant.interface';
import { RestaurantService } from '../services/restaurant.service';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'fs-restaurant-card',
  templateUrl: './restaurant-card.component.html',
  styleUrls: ['./restaurant-card.component.scss']
})
export class RestaurantCardComponent implements OnInit {
  @Input() restaurant: Restaurant;
  @Input() links = true;
  @Output() deleted = new EventEmitter<void>();
  readonly days = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];
  weekDay: number;
  auxRating:number;
  distance:number;

  constructor(private restService: RestaurantService, private router: Router) { }

  ngOnInit() {
    this.weekDay = (new Date()).getDay();
    this.auxRating = this.restaurant.stars;
    console.log(this.restaurant);
  }

  getDaysString() {
    return this.restaurant.daysOpen.map(d => this.days[d]).join(', ');
  }

  delete() {

     swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then(async (result)=>{
      if (result.value) {
        this.restService.deleteRestaurant(this.restaurant.id).subscribe(
          () => this.deleted.emit(),
          error => console.error(error)
        );
      }
    })
   
  }

  edit(){
    if (this.links) {
      this.router.navigate(['restaurants/edit', this.restaurant.id]);
    }
  }

  goDetails() {
    if (this.links) {
      this.router.navigate(['/restaurants/details', this.restaurant.id]);
    }
  }
}
