import { Component, OnInit, Input } from '@angular/core';
import { CommentRestaurant } from '../interfaces/commentRestaurant.interface';

@Component({
  selector: 'fs-restaurant-comment',
  templateUrl: './restaurant-comment.component.html',
  styleUrls: ['./restaurant-comment.component.scss']
})
export class RestaurantCommentComponent implements OnInit {

  @Input() comment: CommentRestaurant;
  constructor() { }

  ngOnInit() {
    
  }

}
