import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RestaurantService } from '../services/restaurant.service';
import { Restaurant } from '../interfaces/restaurant.interface';
import { NgModel, NgForm } from '@angular/forms';
import { CommentRestaurant } from '../interfaces/commentRestaurant.interface';
import swal, { SweetAlertOptions } from 'sweetalert2';

@Component({
  selector: 'fs-restaurant-details',
  templateUrl: './restaurant-details.component.html',
  styleUrls: ['./restaurant-details.component.scss']
})
export class RestaurantDetailsComponent implements OnInit {
  restaurant: Restaurant;
  lat = 38.4039418;
  lng = -0.5288701;
  zoom = 17;
  auxRating = 1;
  rating =1;
  comment_text ="";
  comments:CommentRestaurant[] = [];
  @ViewChild('commentForm') commentForm: NgForm;
  @Output() changeRating = new EventEmitter<number>();
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private restService: RestaurantService
  ) {}

  ngOnInit() {
    this.restaurant = this.route.snapshot.data['restaurant'];
    this.lat = this.restaurant.lat;
    this.lng = this.restaurant.lng;

    this.restService.getCommentRestaurant(this.restaurant.id).subscribe(
      comments=>this.comments = comments,
      error=>console.log("Error"),
      ()=>console.log("Comentarios cargados")
    );
  }

  ngOnChanges() {
    this.auxRating = this.rating;
  }

  change() {
    console.log(this.auxRating+"");
    this.changeRating.emit(this.auxRating);
  }

  addComment() {
      console.log("Comentario añadido");
      if (!this.commentForm.invalid){
          this.restService.addCommentToRestaurant(this.restaurant.id,
            this.rating,this.comment_text).subscribe(
              rest =>{
                 this.comments.push(rest);
                 this.restaurant.commented = true;
                },
              error =>console.log(error)
            );
      }
  }

  goBack() {
    this.router.navigate(['/restaurants']);
  }

  validClasses(ngModel: NgModel, validClass: string, errorClass: string) {
    return {
      [validClass]: ngModel.touched && ngModel.valid,
      [errorClass]: ngModel.touched && ngModel.invalid
    };
  }
}
