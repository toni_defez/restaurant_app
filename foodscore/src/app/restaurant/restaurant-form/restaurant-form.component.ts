import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Restaurant } from '../interfaces/restaurant.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { RestaurantService } from '../services/restaurant.service';
import { NgModel, NgForm } from '@angular/forms';
import { Result } from 'ngx-mapbox-gl/lib/control/geocoder-control.directive';
import swal, { SweetAlertOptions } from 'sweetalert2';
import { CanComponentDeactivate } from 'src/app/guards/can-deactivate.guard';
import { Observable, from, of } from 'rxjs';
import { ConfirmModalComponent } from 'src/app/modals/confirm-modal/confirm-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { switchMap, map, catchError } from 'rxjs/operators';


@Component({
  selector: 'fs-restaurant-form',
  templateUrl: './restaurant-form.component.html',
  styleUrls: ['./restaurant-form.component.scss']
})
export class RestaurantFormComponent implements OnInit, CanComponentDeactivate {
  readonly days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  newRest: Restaurant = null;
  daysOpen: boolean[] = [];
  @Output() add = new EventEmitter<Restaurant>();
  @ViewChild('restForm') restForm: NgForm;
  @ViewChild('fileImage') fileImage: ElementRef;
  @ViewChild('formProd') formProd: NgForm;

  zoom = 17;
  edit = false;

  constructor(private router: Router, private restService: RestaurantService,
    private route: ActivatedRoute, private modalService: NgbModal) {

  }

  ngOnInit() {
    const id = +this.route.snapshot.params.id;
    if (!isNaN(id)) {
      this.newRest = this.route.snapshot.data.restaurant;
      this.edit = true;
      this.daysOpen = (new Array(7)).fill(false);
      for (let i = 0; i < this.newRest.daysOpen.length; i++) {
        this.daysOpen[this.newRest.daysOpen[i]] = true;
      }
    } else {
      this.resetForm();
    }
  }

  resetForm() {
    this.daysOpen = (new Array(7)).fill(true);
    this.fileImage.nativeElement.value = '';
    this.newRest = {
      name: '',
      cuisine: '',
      daysOpen: [],
      description: '',
      image: '',
      phone: '',
      address: 'None',
      lat: 38.4039418,
      lng: -0.5288701
    };
  }

  changeImage(fileInput: HTMLInputElement) {
    if (!fileInput.files || fileInput.files.length === 0) { return; }
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', e => {
      this.newRest.image = <string>reader.result;
    });
  }

  addRestaurant() {

    if (this.restForm.invalid || !this.newRest.image) { return; }

    this.newRest.daysOpen = this.daysOpen.reduce((days, isSelected, i) => isSelected ? [...days, i] : days, []);
    if (!this.edit) {
      this.restService.addRestaurant(this.newRest).subscribe(
        rest => {
          this.router.navigate(['/restaurants']);
        },
        error => console.error(error)
      );
    } else {
      console.log('Actualizando');
      // updateRestaurant
      this.restService.updateRestaurant(this.newRest).subscribe(
        rest => {
          this.newRest = rest;
          swal({
            title: 'Restaurant update!',
            text: 'Your restaurant has been updated'
          });
        },
        error => console.error(error)
      );
    }
  }

  changePosition(result: Result) {
    this.newRest.lat = +result.geometry.coordinates[1];
    this.newRest.lng = +result.geometry.coordinates[0];
    console.log('New address: ' + result.place_name);
  }

  validClasses(ngModel: NgModel, validClass: string, errorClass: string) {
    return {
      [validClass]: ngModel.touched && ngModel.valid,
      [errorClass]: ngModel.touched && ngModel.invalid
    };
  }

  canDeactivate(): Observable<boolean> {

    if (this.restForm.dirty) {
      console.log('formulario tocado');
      const modalRef = this.modalService.open(ConfirmModalComponent);
      modalRef.componentInstance.title = 'Save changes';
      modalRef.componentInstance.body = 'If you leave this page, all changes will be lost. Do you want to save changes?';

      return from(modalRef.result).pipe(
        switchMap(resp => {
          if (resp) {
            if (this.restForm.valid) {
              return this.restService.updateRestaurant(this.newRest).pipe(
                map(p => true),
                catchError(e => {
                  swal({
                    title: 'Profile update!',
                    text: 'Your avatar has been updated'
                  });
                  return of(false);
                })
              );
            } else {
              return of(false);
            }
          } else {
            return of(true);
          }
        }),
        catchError(e => of(false))
      );
    } else {
      console.log('formulario no tocado');
      return of(true);
    }

  }



}
