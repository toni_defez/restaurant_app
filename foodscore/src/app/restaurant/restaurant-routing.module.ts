import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestaurantsPageComponent } from './restaurants-page/restaurants-page.component';
import { LoginActivateGuard } from '../guards/login-activate.guard';
import { RestaurantDetailsComponent } from './restaurant-details/restaurant-details.component';
import { RestaurantResolver } from './resolvers/restaurant.resolver';
import { RestaurantFormComponent } from './restaurant-form/restaurant-form.component';
import { Show } from '../shared/enums/show.enum';
import { CanDeactivateGuard } from '../guards/can-deactivate.guard';

const routes: Routes = [
  {
    path: 'details/:id',
    component: RestaurantDetailsComponent,
    canActivate: [LoginActivateGuard],
    resolve: {
      restaurant: RestaurantResolver
    }
  },
  {
    path:'edit/:id',
    component:RestaurantFormComponent,
    canActivate:[LoginActivateGuard],
    canDeactivate: [CanDeactivateGuard],
    resolve:{
      restaurant:RestaurantResolver
    }
  },
  {
    path: 'new',
    component: RestaurantFormComponent,
  
    canActivate: [LoginActivateGuard]
  }, 
  {
    path: 'mine',
    component: RestaurantsPageComponent,
    canActivate: [LoginActivateGuard],
    data: { show: Show.MINE }
  },
  {
    path: 'user/:id',
    component: RestaurantsPageComponent,
    canActivate: [LoginActivateGuard],
    data: { show: Show.ASSIST }
  },
  {
    path: '',
    component: RestaurantsPageComponent,
    canActivate: [LoginActivateGuard],
    data: { show: Show.ALL }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantRoutingModule { }
