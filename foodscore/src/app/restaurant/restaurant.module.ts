import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RestaurantRoutingModule } from './restaurant-routing.module';
import { ValidatorsModule } from '../validators/validators.module';
import { RestaurantsPageComponent } from './restaurants-page/restaurants-page.component';
import { RestaurantCardComponent } from './restaurant-card/restaurant-card.component';
import { RestaurantFormComponent } from './restaurant-form/restaurant-form.component';
import { RestaurantDetailsComponent } from './restaurant-details/restaurant-details.component';
import { RestaurantFilterPipe } from './pipes/restaurant-filter.pipe';
import { FormsModule } from '@angular/forms';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { RestaurantCommentComponent } from './restaurant-comment/restaurant-comment.component';
import { ModalsModule } from '../modals/modals.module';
import { ConfirmModalComponent } from '../modals/confirm-modal/confirm-modal.component';

@NgModule({
  imports: [
    CommonModule,
    RestaurantRoutingModule,
    FormsModule,
    ValidatorsModule,
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoiYWRpNyIsImEiOiJjanByNGN1Y3QwcW8yNDJuOWwyZW5rOWIxIn0.fpriima12X9VJOoaeZczmg'
    }),
  ],
 
  declarations: [
    RestaurantsPageComponent,
    RestaurantCardComponent,
    RestaurantFormComponent,
    RestaurantFilterPipe,
    RestaurantDetailsComponent,
    RestaurantCommentComponent,
    
  ]
})
export class RestaurantModule { }
