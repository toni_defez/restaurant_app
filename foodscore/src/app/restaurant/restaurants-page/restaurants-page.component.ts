import { Component, OnInit } from '@angular/core';
import { Restaurant } from '../interfaces/restaurant.interface';
import { RestaurantService } from '../services/restaurant.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Show } from '../../shared/enums/show.enum';

@Component({
  selector: 'fs-restaurants-page',
  templateUrl: './restaurants-page.component.html',
  styleUrls: ['./restaurants-page.component.scss']
})
export class RestaurantsPageComponent implements OnInit {
  restaurants: Restaurant[] = [];
  orderByName = false;
  showOpen = false;
  search = '';

  constructor(private restaurantService: RestaurantService ,
    private route: ActivatedRoute,
    private router: Router, private restService: RestaurantService)  { 

    }

  ngOnInit() {
    
    switch(this.route.snapshot.data.show){
      case Show.ALL:
        console.log("todos");
        this.restaurantService.getRestaurants().subscribe(
          rests => this.restaurants = rests
        );
      break;
      case Show.MINE:
        console.log("mios");
       
        this.restaurantService.getRestaurantMine().subscribe(
          rests => this.restaurants = rests
        );
      break;
      case Show.ASSIST:
        let id = this.route.snapshot.paramMap.get("id");
        this.restaurantService.getRestaurantsByUser(+id).subscribe(
          rests => this.restaurants = rests
        );
      break;
    }

  }

  toggleOrderName(event: Event) {
    event.preventDefault();
    this.orderByName = !this.orderByName;
  }

  toggleShowOpen(event: Event) {
    event.preventDefault();
    this.showOpen = !this.showOpen;
  }

  delete(rest: Restaurant) {
    const i = this.restaurants.indexOf(rest);
    this.restaurants = this.restaurants.slice(0, i).concat(this.restaurants.slice(i + 1));
  }

  add(rest: Restaurant) {
    this.restaurants = [...this.restaurants, rest];
  }
}
