import { Injectable } from '@angular/core';
import { Restaurant } from '../interfaces/restaurant.interface';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Identifiers } from '@angular/compiler';
import { CommentRestaurant } from '../interfaces/commentRestaurant.interface';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  constructor(private http: HttpClient) {}

  getRestaurants(): Observable<Restaurant[]> {
    return this.http.get<{ restaurants: Restaurant[] }>('restaurants').pipe(
      map(resp =>
        resp.restaurants.map(r => {
          r.image = `${environment.baseUrl}/${r.image}`;
          r.distance = Math.round(r.distance * 100) / 100;
          return r;
        })
      )
    );
  }

  getRestaurantMine():Observable<Restaurant[]>{
    return this.http.get<{ restaurants: Restaurant[] }>('restaurants/mine').pipe(
      map(resp =>
        resp.restaurants.map(r => {
          r.image = `${environment.baseUrl}/${r.image}`;
          r.distance = Math.round(r.distance * 100) / 100;
          return r;
        })
      )
    );
  }

  getRestaurantsByUser(id:number){
    return this.http.get<{ restaurants: Restaurant[] }>(`restaurants/user/${id}`).pipe(
      map(resp =>
        resp.restaurants.map(r => {
          r.image = `${environment.baseUrl}/${r.image}`;
          r.distance = Math.round(r.distance * 100) / 100;
          return r;
        })
      )
    );
  }

  getRestaurant(id: number): Observable<Restaurant> {
    return this.http.get<{ restaurant: Restaurant }>(`restaurants/${id}`).pipe(
      map(resp => {
        const r = resp.restaurant;
        r.image = `${environment.baseUrl}/${r.image}`;
        r.distance = Math.round(r.distance * 100) / 100;
        return r;
      })
    );
  }

  addRestaurant(rest: Restaurant): Observable<Restaurant> {
    return this.http.post<{ restaurant: Restaurant }>(`restaurants`, rest).pipe(
      map(resp => {
        const r = resp.restaurant;
        r.image = `${environment.baseUrl}/${r.image}`;
        return r;
      })
    );
  }

  updateRestaurant(rest:Restaurant):Observable<Restaurant>{
    
    return this.http.put<{restaurant:Restaurant}>(`restaurants/${rest.id}`,{
      "name":rest.name,
      "description":rest.description,
      "daysOpen":rest.daysOpen,
      "cuisine":rest.cuisine,
      "phone":rest.phone,
      "address": rest.address,
      "lat": +rest.lat,
      "lng": +rest.lng,
      "image": rest.image
    }).pipe(
      map(resp=>{
        const r = resp.restaurant;
        r.image = `${environment.baseUrl}/${r.image}`;
        return r;
      })
    );
  }


  addCommentToRestaurant(idRestaurant:number,stars:number,
    text:string){
      return this.http.post<{comment:CommentRestaurant}>(`restaurants/${idRestaurant}/comments`,
      {
        "stars":stars,
        "text":text
      }).pipe(
        map(resp=>{
          const r = resp.comment;
          r.user.avatar = `${environment.baseUrl}/${r.user.avatar}`;
          return r;
        })
      )
    }

  getCommentRestaurant(idRestaurant:number){
    return this.http.get<{comments:CommentRestaurant[]}>(`restaurants/${idRestaurant}/comments`).pipe(
        map(resp=>{
          const comments = resp.comments;
          comments.map(c=>{
            c.user.avatar = `${environment.baseUrl}/${c.user.avatar}`;
          })
          return comments;
        })
    )
  }

  deleteRestaurant(id: number): Observable<void> {
    return this.http.delete<void>(`restaurants/${id}`);
  }
}
