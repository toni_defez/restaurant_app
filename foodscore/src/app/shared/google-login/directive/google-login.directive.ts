import { Directive, Output, EventEmitter, ElementRef } from '@angular/core';
import { LoadGoogleApiService } from '../services/load-google-api.service';

@Directive({
  selector: '[fsGoogleLogin]'
})

/*
Client ID:

807741492839-apvrne0j65a0hfv4o6n9fj80ko7omv35.apps.googleusercontent.com

Client Secret:
JGkTtBd4CMNLZcPhAINraUYO


*/
export class GoogleLoginDirective {

  @Output() loginOk: EventEmitter<gapi.auth2.GoogleUser> =
    new EventEmitter<gapi.auth2.GoogleUser>();
  @Output() loginError: EventEmitter<string> = new EventEmitter<string>();
  @Output() loadingEnd: EventEmitter<void> = new EventEmitter<void>();

  constructor(
    private el: ElementRef,
    private loadService: LoadGoogleApiService
  ) { }

  ngOnInit() {
    this.loadService.getAuthApi().subscribe(
      auth2 => {
        auth2.attachClickHandler(
          this.el.nativeElement,
          {},
          user => this.loginOk.emit(user),
          error => this.loginError.emit(error)
        );
        this.loadingEnd.emit();
      }
    );
  }
}
