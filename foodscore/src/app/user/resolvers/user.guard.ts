import { Injectable } from '@angular/core';
import {  ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { User } from 'src/app/auth/interfaces/user.interface';
import { UserService } from '../services/user.service';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements Resolve<User> {
  constructor(private userService: UserService, private router: Router) {}
  
  resolve(route: ActivatedRouteSnapshot): Observable<User> {
 
      return this.userService.getUser(route.params['id']).pipe(
        catchError(error => {
          this.router.navigate(['/restaurants']);
          return of(null);
        })
      );
    

  }
}
