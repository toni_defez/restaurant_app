import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/auth/interfaces/user.interface';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}

  getUser(id: number=null): Observable<User> {
    console.log(id);
    let ide = id==null?'me':id;
    console.log(ide);
    return this.http.get<{ user: User }>(`users/${ide}`).pipe(
      map(resp => {
        const r = resp.user;
        r.avatar = `${environment.baseUrl}/${r.avatar}`;
        return r;
      })
    );
  }

  changeUserProfile(user:User):Observable<User>{
    return this.http.put(`users/me`, 
    {
      "name":user.name,"email":user.email
    }).pipe(
      map(resp => {
        const r = resp;
        return r;
      })
    );
  }

  changeAvatarProfile(avatar:string):Observable<User>{
    return this.http.put(`users/me/avatar`,
    {
      "avatar":avatar
    }).pipe(map(resp=>{
        const r = resp;
        return r;
    }))
  }

  changePasswordProfile(user:User):Observable<User>{
    return this.http.put(`users/me/password`,
    {
      "password":user.password
    }).pipe(map(resp=>{
      const r = resp;
      return r;
    })

    );
  }
}
