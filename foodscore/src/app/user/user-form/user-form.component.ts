import { Component, OnInit, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/auth/interfaces/user.interface';
import { NgModel, NgForm } from '@angular/forms';
import { UserService } from '../services/user.service';
import swal, { SweetAlertOptions } from 'sweetalert2';


@Component({
  selector: 'fs-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  user: User;
  newImage : string;
  change:boolean=false;
  @ViewChild('profileForm') profileForm: NgForm;
  @ViewChild('avatarForm') avatarForm: NgForm;
  @ViewChild('passwordForm') passwordForm : NgForm;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private userService: UserService
  ) { }

  ngOnInit() {
    swal.isVisible();
    this.user = this.route.snapshot.data['user'];
    console.log(this.user);
    this.user.repeatPassword
  }

  validClasses(ngModel: NgModel, validClass: string, errorClass: string) {
    return {
      [validClass]: ngModel.touched && ngModel.valid,
      [errorClass]: ngModel.touched && ngModel.invalid
    };
  }

  changeProfile(){
    if (this.profileForm.invalid )return;

    this.userService.changeUserProfile(this.user).subscribe(
      rest => {
        swal({
         title: "Profile update!",
         text: "Your profile has been updated"
       })
     },
      error => console.error(error)
    );
  }

  changeImage(fileInput: HTMLInputElement) {
    if (!fileInput.files || fileInput.files.length === 0) { return; }
    const reader: FileReader = new FileReader();
    reader.readAsDataURL(fileInput.files[0]);
    reader.addEventListener('loadend', e => {
      this.newImage = <string>reader.result;
      this.change = true;
    });
  }

  changeAvatar(){
    if(this.avatarForm.invalid)return;

    this.userService.changeAvatarProfile(this.newImage).subscribe(
       rest =>{
         swal({
          title: "Profile update!",
          text: "Your avatar has been updated"
        }).then(()=>{
          this.user.avatar = this.newImage;
          this.newImage = "";
          this.change = false;
        })
      },
      error => console.log(error)
    );

  }

  changePassword(){
    if(this.passwordForm.invalid)return;

    this.userService.changePasswordProfile(this.user).subscribe(
      rest =>{
        swal({
         title: "Profile update!",
         text: "Your password has been updated"
       })
     },
      error=>console.log(error)
    );
  }



}
