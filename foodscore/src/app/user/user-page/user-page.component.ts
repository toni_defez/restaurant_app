import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/auth/interfaces/user.interface';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'fs-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  lat = 38.4039418;
  lng = -0.5288701;
  zoom = 17;
  user :User;
  constructor( private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = this.route.snapshot.data['user'];
    console.log(this.user);
    this.lat = this.user.lat;
    this.lng = this.user.lng;
   
  }

}
