import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserPageComponent } from './user-page/user-page.component';
import { UserGuard } from './resolvers/user.guard';
import { UserFormComponent } from './user-form/user-form.component';

const routes: Routes = [
  {
    path: 'me',
    component: UserPageComponent,
    pathMatch: 'full',
    resolve:{
      user:UserGuard
    }
  },
  {
    path: 'edit',
    pathMatch:'full',
    component:UserFormComponent,
    resolve:{
      user:UserGuard
    }
  },
  {
    path: ':id',
    component:UserPageComponent,
    pathMatch:'full',
    resolve:{
      user:UserGuard
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
