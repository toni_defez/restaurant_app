import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserPageComponent } from './user-page/user-page.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { UserFormComponent } from './user-form/user-form.component';
import { FormsModule } from '@angular/forms';
import { ValidatorsModule } from '../validators/validators.module';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    NgxMapboxGLModule.withConfig({
      accessToken: 'pk.eyJ1IjoiYWRpNyIsImEiOiJjanByNGN1Y3QwcW8yNDJuOWwyZW5rOWIxIn0.fpriima12X9VJOoaeZczmg'
    }),
    ValidatorsModule
  ],
  declarations: [
    UserPageComponent,
    UserFormComponent
  ]
})
export class UserModule { }
