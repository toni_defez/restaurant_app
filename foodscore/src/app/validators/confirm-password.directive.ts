import { Directive, Input } from '@angular/core';
import { Validator, NG_VALIDATORS, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[fsConfirmPassword]',
  providers: [{provide: NG_VALIDATORS, useExisting: ConfirmPasswordDirective, multi: true}]
})
export class ConfirmPasswordDirective implements Validator {

  constructor() { }

  @Input() fsConfirmPassword:string;
  validate(group: AbstractControl): { [key: string]: any } {
    const controlToCompare = group.parent.get(this.fsConfirmPassword);
    if(controlToCompare && controlToCompare.value !== group.value)
    {
      return { 'notEqual':true}
    }

    return null; // No errors
  }


}
