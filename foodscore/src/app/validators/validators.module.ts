import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OneCheckedDirective } from './one-checked.directive';
import { ConfirmPasswordDirective } from './confirm-password.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    OneCheckedDirective,
    ConfirmPasswordDirective
  ],
  exports: [
    OneCheckedDirective,
    ConfirmPasswordDirective
  ]
})
export class ValidatorsModule { }
